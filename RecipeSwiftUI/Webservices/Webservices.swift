//
//  Webservices.swift
//  RecipeSwiftUI
//
//  Created by rafiul hasan on 21/9/21.
//

import Foundation

enum NetworkError: Error {
    case badRequest
    case decodingError
}

class Webservice {
    func get<T: Decodable>(url: URL, parse: (Data) -> T?) async throws -> T {
        let (data, response) = try await URLSession.shared.data(from: url)
        
        if (response as? HTTPURLResponse)?.statusCode != 200 {
            throw NetworkError.badRequest
        }
        
        guard let result = parse(data) else {
            throw NetworkError.decodingError
        }
        
        return result
    }
}


class Webservices {
    static func getRecipeCategory(urlString: URL, completion: @escaping ( Result<[RecipeCategory], Error>) -> Void) {
        URLSession.shared.dataTask(with: urlString) { data, response, error in
            if let err = error {
                completion(Result.failure(err))
            }
            guard let data = data else { return }
            
            let decoder = JSONDecoder()
            do {
                let jsonCategory = try? decoder.decode([RecipeCategory].self, from: data)
                DispatchQueue.main.async {
                    completion(Result.success(jsonCategory!))
                }
            } catch {
                completion(Result.failure(error))
            }
        }.resume()
    }
    
    static func getRecipeByName(urlString: String, completion: @escaping(RecipeResponse?, Error?) -> Void ) {
        let url = URL(string: urlString)
        guard let url = url else { return }
        
        URLSession.shared.dataTask(with: url) { data, response, error in
            if let err = error {
                completion(nil, err)
            }
            guard let data = data else { return }
            
            let decoder = JSONDecoder()
            do {
                let jsonRecipe = try? decoder.decode(RecipeResponse.self, from: data)
                completion(jsonRecipe, nil)
            } catch {
                print(error)
            }
        }.resume()
    }
    
    static func getRecipeDetail(urlString: String, completion: @escaping(RecipeDetailResponse?, Error?) -> Void ) {
        let url = URL(string: urlString)
        guard let url = url else { return }
        
        URLSession.shared.dataTask(with: url) { data, response, error in
            if let err = error {
                completion(nil, err.localizedDescription as? Error)
            }
            guard let data = data else { return }
            
            let decoder = JSONDecoder()
            do {
                let jsonRecipeDetail = try? decoder.decode(RecipeDetailResponse.self, from: data)
                completion(jsonRecipeDetail, nil)
            } catch {
                print(error.localizedDescription)
            }
        }.resume()
    }
}
