//
//  RecipeSwiftUIApp.swift
//  RecipeSwiftUI
//
//  Created by rafiul hasan on 21/9/21.
//

import SwiftUI

@main
struct RecipeSwiftUIApp: App {
    var body: some Scene {
        WindowGroup {
            RecipeCategoryListScreen()
        }
    }
}
