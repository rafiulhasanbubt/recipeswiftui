//
//  RecipeDetailViewModel.swift
//  RecipeSwiftUI
//
//  Created by rafiul hasan on 21/9/21.
//

import Foundation

//class RecipeDetailViewModel: ObservableObject {
//    let service = Webservices()
//    @Published var recipeDetail: RecipeDetailResponse?
//    @Published var recipeId: String = ""
//    @Published var imageUrl: URL?
//    @Published var title: String = ""
//    @Published var ingredients: [String] = []
//
//
//}

@MainActor
class RecipeDetailViewModel: ObservableObject {
    
    @Published var imageUrl: URL?
    @Published var title: String = ""
    @Published var ingredients: [String] = []
    
    func populateRecipeDetail(recipeId: String) async {
        do {
            let recipeDetailResponse = try await Webservice().get(url: Constants.Urls.recipeById(recipeId)) { data in
                return try? JSONDecoder().decode(RecipeDetailResponse.self, from: data)
            }
            
            let recipeDetail = recipeDetailResponse.recipe
            self.imageUrl = URL(string: recipeDetail.imageUrl)!
            self.title = recipeDetail.title
            self.ingredients = recipeDetail.ingredients
            
        } catch {
            print(error)
        }
    }
}
