//
//  RecipeViewModel.swift
//  RecipeSwiftUI
//
//  Created by rafiul hasan on 21/9/21.
//

import Foundation

struct RecipeViewModel {
    
    private let recipe: Recipe
    
    init(_ recipe: Recipe) {
        self.recipe = recipe
    }
    
    var id: String {
        recipe.id
    }
    
    var title: String {
        recipe.title
    }
    
    var imageURL: URL? {
        URL(string: recipe.imageUrl)
    }
}
