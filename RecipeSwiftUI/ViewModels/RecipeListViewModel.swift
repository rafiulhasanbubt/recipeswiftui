//
//  RecipeListViewModel.swift
//  RecipeSwiftUI
//
//  Created by rafiul hasan on 21/9/21.
//

import SwiftUI

@MainActor
class RecipeListViewModel: ObservableObject {
    
    @Published var recipes: [RecipeViewModel] = [RecipeViewModel]()
    
    func populateRecipesByCategory(name: String) async {        
        do {
            let recipeResponse = try await Webservice().get(url: Constants.Urls.recipeByCategoryName(name)) { data in
                return try? JSONDecoder().decode(RecipeResponse.self, from: data)
            }
            self.recipes = recipeResponse.recipes.map(RecipeViewModel.init)
        } catch {
            print(error)
        }
    }
}
