//
//  ContentView.swift
//  RecipeSwiftUI
//
//  Created by rafiul hasan on 21/9/21.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        Text("Hello, world!")
            .padding()
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
