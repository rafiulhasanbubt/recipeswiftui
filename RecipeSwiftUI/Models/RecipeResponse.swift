//
//  RecipeResponse.swift
//  RecipeSwiftUI
//
//  Created by rafiul hasan on 21/9/21.
//

import Foundation

struct RecipeResponse: Decodable {
    let recipes: [Recipe]
}

struct Recipe: Decodable {
    let id: String
    let imageUrl: String
    let title: String
}
