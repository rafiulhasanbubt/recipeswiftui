//
//  RecipeDetailResponse.swift
//  RecipeSwiftUI
//
//  Created by rafiul hasan on 21/9/21.
//

import Foundation

struct RecipeDetailResponse: Decodable {
    let recipe: RecipeDetail
}

struct RecipeDetail: Decodable {
    let imageUrl: String
    let id: String
    let title: String
    let ingredients: [String]
}
